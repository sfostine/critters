/***
 * @author Samuel Fostine
 * Date of modification : March 4, 2015
 * This class extends the critter class and complete the Critter class by adding the image for a critter
 * And it also provides the method to draw a critter
 */
package critter;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;

public class CompleteCritter extends Critter {

	private String path;
	protected Image crit;
	private int level;
	

	// extends critter class, and add the image for a critter in different level
	public CompleteCritter(int gameLevel, String imagePath) {
		super(gameLevel);
		this.level = gameLevel;
		this.path = imagePath;
		loadImage();
		
	}

	/*** Load the sprite and assign ot to the Image variable */
	private void loadImage() {
		ImageIcon image = new ImageIcon(path);
		crit = image.getImage();
	}

	/***
	 * draw critters at specific coordinates and some String on the screen
	 * 
	 * @param g
	 */
	public void drawCritters(Graphics g) {
		g.drawImage(crit, this.getX(), getY(), null);
		// draw the level
		int x = super.coord.measure.getWidth() / 2 + 100;
		int y = 40;
		g.setColor(Color.MAGENTA);
		g.drawString("LEVEL: " + level, x, y);
		g.drawString("SPEED: " + this.getSpeed(), x, y + 20);
		g.drawString("REWARD: " + this.getreward(), x, y + 40);
		g.drawString("STRENGTH: " + this.getStrength(), x, y + 60);
		Toolkit.getDefaultToolkit().sync();

	}
	
	


}
