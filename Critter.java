/***
 * @author Samuel Fostine
 * Date of modification : March 4, 2015
 * This class contains all the attributes of a critter 
 * and the functions that will be used to access or modify the attributes
 */
package critter;

public class Critter {
	// private attributes to each critter
	private int reward, hitPoint, strength, level, speed;

	// coordinate of critters
	private int x, y;

	// position of the critters
	protected CrittersPosition coord = new CrittersPosition();

	// NB: attributes are dependent on the game level
	public Critter(int gameLevel) {
		this.level = gameLevel;
		this.reward = gameLevel;
		this.hitPoint = gameLevel * 2;
		this.strength = gameLevel;
		this.speed = gameLevel*3+1;
		this.x = coord.getX();
		this.y = coord.getY();
		
	}

	/***
	 * reduce the hit points of the critters. Reduction variable allows to
	 * remove a certain point to the critter dependent on the tower, because a
	 * tower can shoot stronger than another one .
	 * 
	 * @param reduction
	 */
	public void updatePoints(int reduction) {
		hitPoint -= reduction;
	}

	/***
	 * @return boolean true if the hitpoint of critter is 0, false otherwise
	 */
	public boolean isDead() {
		return hitPoint == 0;
	}

	/***
	 * @return int reward for the user when killing a critter
	 */
	public int getreward() {
		return reward;
	}

	/***
	 * @return int get the strength of the critter to remove coin to the user
	 */
	public int getStrength() {
		return strength;
	}

	/***
	 * @return int the speed of the critter dependent on the level
	 * */
	public int getSpeed() {
		return speed;
	}

	/***
	 * 
	 * @return int the hitpoint of the critter
	 */
	public int getHitPoint() {
		return hitPoint;
	}
	
	public void move(VirtualMap map)
	{
		int adjust = 10;// adjust the display of the critters on the map
		if (getX() >= (map.getNewX() + adjust)
				&& getY() <= (map.getStartY() + map.getNewheight()))
			this.setY(getY() + getSpeed());
		else
			setX(getX() + getSpeed());
	}

	/********************** GETTERS AND SETTERS FOR THE COORDINATE OF A CRITTER ************************/
	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}
	
	
}
