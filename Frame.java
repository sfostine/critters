/***
 * @author Samuel Fostine
 * Date of modification : March 4, 2015
 * This class allows to see the screen for the game
 */
package critter;

import java.awt.*;

import javax.swing.*;

public class Frame extends JFrame {

	private static String title = "Tower Defense";
	private int width, height;
	protected Measures measures = Measures.getMeasures();

	private static Dimension size;

	public Frame() {
		init();
	}

	private void init() {
		this.width = measures.getWidth();
		this.height = measures.getHeight();
		size = new Dimension(width, height);
		add(new Screen());
		this.setTitle(title);
		this.setSize(size);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}
}
