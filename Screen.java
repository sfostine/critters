/***
 * @author Samuel Fostine
 * Date of modification : March 4, 2015
 * This class brings every component to the screen, it updates the coordinates of the critters
 * It draws different critters that will be used for different level in the game
 */
package critter;

import java.awt.*;

import javax.swing.JPanel;

public class Screen extends JPanel implements Runnable {
	private Thread thread;
	// the number of critters for level
	private int numCritters_l1 = 30;
	private int numCritters_l2 = 40;
	private int numCritters_l3 = 50;

	// variable to indicate the level
	private boolean lev1, lev2, lev3;

	// three types of critters with different number of critters for each level
	protected CompleteCritter crit1[] = new CompleteCritter[numCritters_l1];
	protected CompleteCritter crit2[] = new CompleteCritter[numCritters_l2];
	protected CompleteCritter crit3[] = new CompleteCritter[numCritters_l3];

	//protected Measures measure = new Measures();
	protected VirtualMap map = new VirtualMap();


	/*********************************************************************/

	public Screen() {
		// init the critters by indicating critter, the level, and the image to
		// use
		initCritter(crit1, 1, numCritters_l1, "src/critter/enemy1.png");
		initCritter(crit2, 2, numCritters_l2, "src/critter/enemy2.png");
		initCritter(crit3, 3, numCritters_l3, "src/critter/enemy3.png");

		// always start with level1 set to true and the others to false
		this.lev1 = true;
		this.lev2 = false;
		this.lev3 = false;

		this.setBackground(Color.CYAN);
		thread = new Thread(this);
		thread.start();

	}

	/*** init a critter that is passed as argument */
	private void initCritter(Critter[] crit, int level, int numberOfCritters,
			String path) {
		// for each level level the first critter will star at -30
		int update = 30;

		// this variable will allow to put the critters at a distance of 30
		int previousX = map.getStartX();
		int adjustY = 5;// this 5 is to adjust the start of the critters in Y
						// coordinate

		for (int i = 0; i < numberOfCritters; i++) {
			crit[i] = new CompleteCritter(level, path);
			crit[i].setX(previousX);
			crit[i].setY(map.getStartY() + adjustY);
			previousX -= update;
		}
	}

	/***
	 * @param crit
	 * @param numberOfCritters
	 * 
	 *            update the list of critters for a specific level the critters
	 *            will start in the same X coordinate as the map, then move
	 *            through the way at the speed of the level
	 */
	private void update(Critter[] crit, int numberOfCritters) {
		int adjust = 10;// adjust the display of the critters on the map
		for (int j = 0; j < numberOfCritters; j++) {
			crit[j].move(map);
			/*if (crit[j].getX() >= (map.getNewX() + adjust)
					&& crit[j].getY() <= (map.getStartY() + map.getNewheight()))
				crit[j].setY(crit[j].getY() + crit[j].getSpeed());
			else
				crit[j].setX(crit[j].getX() + crit[j].getSpeed());*/
		}
	}

	/***
	 * draw the path through the window
	 * 
	 * @param g
	 */
	private void drawPath(Graphics g) {
		g.clearRect(map.getStartX(), map.getStartY(), map.getWidth(), map.getHeight());
		g.clearRect(map.getNewX(), map.getStartY(), map.getHeight(), map.getNewheight());
		g.clearRect(map.getNewX(), map.getStartY()+ map.getNewheight(), map.getMeasure().getWidth(), map.getHeight());
		Toolkit.getDefaultToolkit().sync();

	}

	/***
	 * Update the boolean level to know which level we are supposed to go to and
	 * update with the right parameters
	 * 
	 * @param l1
	 * @param l2
	 * @param l3
	 */
	private void updatelevel(boolean l1, boolean l2, boolean l3) {
		lev1 = l1;
		lev2 = l2;
		lev3 = l3;
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		drawPath(g);

		/*
		 * determine the number of critters to draw and which category of
		 * critters dependent on the level Then update the boolean lev1, lev2,
		 * lev3 to follow to next level when all the critters from the previous
		 * level has gone
		 */
		if (lev1) {
			for (int i = 0; i < numCritters_l1; i++) {
				crit1[i].drawCritters(g);
				if (crit1[numCritters_l1 - 1].getX() >= map.getMeasure().getWidth())
					updatelevel(false, true, false);
			}

		} else if (lev2) {
			for (int i = 0; i < numCritters_l2; i++) {
				crit2[i].drawCritters(g);
				if (crit2[numCritters_l2 - 1].getX() >= map.getMeasure().getWidth())
					updatelevel(false, false, true);
			}
		} else if (lev3) {
			for (int i = 0; i < numCritters_l3; i++) {
				crit3[i].drawCritters(g);
				if (crit3[numCritters_l3 - 1].getX() >= map.getMeasure().getWidth())
					updatelevel(false, false, false);
			}
		}

	}

	@Override
	public void run() {
		while (true) {
			/*
			 * determine the number of critters to draw and which category of
			 * critters dependent on the level Then update the boolean lev1,
			 * lev2, lev3 to follow to next level
			 */
			if (lev1)
				update(crit1, numCritters_l1);
			else if (lev2)
				update(crit2, numCritters_l2);
			else if (lev3)
				update(crit3, numCritters_l3);
			repaint();
			try {
				thread.sleep(200);
			} catch (InterruptedException e) {
			}
		}
	}
}
