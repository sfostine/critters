/***
 * @author Samuel Fostine
 * Date of modification : March 4, 2015
 * This class tests the methods in the program.
 */
package critter;

public class Driver {
	public static void main(String[] args) {
		/***** CHANGE THE LEVEL VARIABLE TO TEST FOR EACH LEVEL ******/
		int level = 1;

		Critter critter = new Critter(level);

		/************
		 * COMMENT THE FRAME IF YOU DON'T WANT TO SEE THE
		 * WINDOW****************************** NB: I implemented the frame with
		 * 3 levels for each of them I use different image, different speed, and
		 * different numbers of critters
		 */
		Frame frame = new Frame();

		// test reward for a critter at a specific level
		int comp1 = critter.getreward();
		int comp2 = level;
		System.out.println("Reward --> Result : " + comp1 + ", Expected: "
				+ comp2 + " " + result(comp1, comp2));

		// test hit point
		comp1 = critter.getHitPoint();
		comp2 = level * 2;
		System.out.println("Hit Point --> Result : " + comp1 + ", Expected: "
				+ comp2 + " " + result(comp1, comp2));

		// update once the hit point after shooting at critter
		/*
		 * the variable towerstrength will indicate how many points we should
		 * remove to the critter. A powerful tower will remove more hit points
		 * than a weak one
		 */
		int towerstrength = 1;
		critter.updatePoints(towerstrength);
		comp1 = critter.getHitPoint();
		comp2 = level * 2 - towerstrength;
		System.out
				.println("After shooting once at critter, the new Hit Point --> Result : "
						+ comp1
						+ ", Expected: "
						+ comp2
						+ " "
						+ result(comp1, comp2));

		// test the strength of the critter
		// the strength must be equal to levelthe same as the level for the test
		comp1 = critter.getStrength();
		comp2 = level;
		System.out.println("Strength of Critter --> Result : " + comp1
				+ ", Expected: " + comp2 + " " + result(comp1, comp2));

		// test the speed of the critter, it should be the same as the level
		comp1 = critter.getSpeed();
		comp2 = level * 3 + 1;
		System.out.println("Speed of Critter --> Result : " + comp1
				+ ", Expected: " + comp2 + " " + result(comp1, comp2));

		// test if a critter is dead of not
		boolean compare1 = critter.isDead();
		boolean compare2 = critter.getHitPoint() == 0;
		System.out.println("Is critter dead --> Result : " + compare1
				+ ", Expected: " + compare2 + " " + result(compare1, compare2));

	}

	/***
	 * indicate if a test is passed or failed
	 * 
	 * @param compare1
	 * @param compare2
	 * @return String indicated if the test pass or fail
	 */
	private static String result(Object compare1, Object compare2) {
		if (compare1.equals(compare2))
			return " ----> Pass";
		else
			return "---> Fail";
	}

}
